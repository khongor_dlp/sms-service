package com.vertexmon.sms.controller;

import com.sipios.springsearch.anotation.SearchSpec;
import com.vertexmon.sms.model.Operator;
import com.vertexmon.sms.service.OperatorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("operator")
public class OperatorController {

    @Autowired
    OperatorService operatorService;

    @GetMapping("/get")
    public ResponseEntity<?> get(@SearchSpec Specification<Operator> specs,
                                 Pageable pageable) {
        Page<Operator> operators = operatorService.get(specs, pageable);

        return ResponseEntity.ok().body(operators);
    }
}
