package com.vertexmon.sms.controller;

import com.sipios.springsearch.anotation.SearchSpec;
import com.vertexmon.sms.model.UserRole;
import com.vertexmon.sms.service.UserRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("userRole")
public class UserRoleController {

    @Autowired
    UserRoleService userRoleService;

    @GetMapping("/get")
    public ResponseEntity<?> get(@SearchSpec Specification<UserRole> specs,
                                 Pageable pageable) {
        Page<UserRole> userRoles = userRoleService.get(specs, pageable);

        return ResponseEntity.ok().body(userRoles);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<?> getById(@PathVariable(value = "id") Long id) {
        UserRole userRole = userRoleService.getById(id);

        return ResponseEntity.ok().body(userRole);
    }
}
