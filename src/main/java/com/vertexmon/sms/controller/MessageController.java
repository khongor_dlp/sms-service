package com.vertexmon.sms.controller;

import com.sipios.springsearch.anotation.SearchSpec;
import com.vertexmon.sms.dto.MessageDTO;
import com.vertexmon.sms.model.*;
import com.vertexmon.sms.repository.MessageRepository;
import com.vertexmon.sms.service.*;
import com.vertexmon.sms.util.Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("message")
public class MessageController {

    @Autowired
    MessageService messageService;

    @Autowired
    OperatorNumberService operatorNumberService;

    @Autowired
    CompanyService companyService;

    @Autowired
    MessageBotService messageBotService;

    @Autowired
    MessageStatusService messageStatusService;

    @Autowired
    MessageRepository messageRepository;

    @GetMapping("/get")
    public ResponseEntity<?> get(@SearchSpec Specification<Message> specs,
                                 Pageable pageable) {
        Page<Message> messages = messageService.get(specs, pageable);

        return ResponseEntity.ok().body(messages);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<?> get(@PathVariable(value = "id") Long id) {
        Message message = messageService.getById(id);

        return ResponseEntity.ok().body(message);
    }

    @GetMapping("/receive")
    public ResponseEntity<?> save(@RequestParam String number,
                                  @RequestParam String content) {
        if (number == null || number.length() != 8) {
            return ResponseEntity.badRequest().body("Утасны дугаар хоосон байна!");
        }

        if (content == null || content.equals("")) {
            return ResponseEntity.badRequest().body("Агуулга хоосон байна!");
        }

        OperatorNumber operatorNumber = operatorNumberService.findByNumber(Long.valueOf(number.substring(0, 2)));

        if (operatorNumber == null) {
            return ResponseEntity.badRequest().body("Үүрэн холбоо олдсонгүй!");
        }

        MessageStatus messageStatus = messageStatusService.findByCode(Util.messageStatusReceivedCode);

        if (messageStatus == null) {
            return ResponseEntity.badRequest().body("Зурвасын төлөв олдсонгүй!");
        }

        Message message = new Message();

        /*if (content.split(" ")[0] != null) {
            Company company = companyService.findByKeyword(content.split(" ")[0].toLowerCase());

            message.setCompany(company);
        }*/

        message.setNumber(Long.valueOf(number));
        message.setContent(content);
        message.setMessageStatus(messageStatus);
        message.setOperator(operatorNumber.getOperator());
        message.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        message.setIsRead(false);

        messageService.save(message);

        MessageBot messageBot = messageBotService.findByQuestion(content);

        if (messageBot != null) {
            Message message1 = new Message();

            message1.setNumber(message.getNumber());
            message1.setContent(messageBot.getAnswer());
            message1.setOperator(message.getOperator());
            message1.setCreatedDate(new Timestamp(System.currentTimeMillis()));

            if (message.getCompany() != null) {
                message1.setCompany(message.getCompany());
            }

            String messageStatusCode = messageService.send(message1.getOperator().getName(), message1.getNumber(), message1.getContent());
            messageStatus = messageStatusService.findByCode(messageStatusCode);
            message1.setMessageStatus(messageStatus);

            if (messageStatusCode.equals(Util.messageStatusSentCode)) {
                message1.setSentDate(new Timestamp(System.currentTimeMillis()));
            }

            messageService.save(message1);
        }

        return ResponseEntity.ok().body(message);
    }

    @PutMapping("/read/{id}")
    public ResponseEntity<?> read(@PathVariable(value = "id") Long id) {
        Message message = messageService.getById(id);

        message.setIsRead(true);

        messageService.save(message);

        return ResponseEntity.ok().body(message);
    }

    @GetMapping("/delay/{id}")
    public ResponseEntity<?> delay(@PathVariable(value = "id") Long id) {
        Message message = messageService.getById(id);

        MessageStatus messageStatus = messageStatusService.findByCode(Util.messageStatusDelayedCode);

        message.setMessageStatus(messageStatus);

        messageService.save(message);

        return ResponseEntity.ok().body(message);
    }

    @GetMapping("/resend/{id}")
    public ResponseEntity<?> resend(@PathVariable(value = "id") Long id) {
        Message message = messageService.getById(id);

        String messageStatusCode = messageService.send(message.getOperator().getName(), message.getNumber(), message.getContent());

        if (messageStatusCode.equals(Util.messageStatusSentCode)) {
            MessageStatus messageStatus = messageStatusService.findByCode(messageStatusCode);
            message.setMessageStatus(messageStatus);
            message.setSentDate(new Timestamp(System.currentTimeMillis()));

            messageService.save(message);

            return ResponseEntity.ok().body(message);
        }

        return ResponseEntity.badRequest().body(message);
    }

    @PostMapping("/send")
    public ResponseEntity<?> send(@RequestBody Message message) {
        if (message.getNumber() == null || message.getNumber().toString().length() != 8) {
            return ResponseEntity.badRequest().body("Утасны дугаар хоосон байна!");
        }

        if (message.getContent() == null || message.getContent().equals("")) {
            return ResponseEntity.badRequest().body("Агуулга хоосон байна!");
        }

        OperatorNumber operatorNumber = operatorNumberService.findByNumber(Long.valueOf(message.getNumber().toString().substring(0, 2)));

        if (operatorNumber == null) {
            return ResponseEntity.badRequest().body("Үүрэн холбоо олдсонгүй!");
        }

        MessageStatus messageStatus = messageStatusService.findByCode(Util.messageStatusScheduledCode);

        if (message.getScheduleDate() != null) {
            message.setMessageStatus(messageStatus);
        } else {
            String messageStatusCode = messageService.send(operatorNumber.getOperator().getName(), message.getNumber(), message.getContent());

            messageStatus = messageStatusService.findByCode(messageStatusCode);
            message.setMessageStatus(messageStatus);

            if (messageStatusCode.equals(Util.messageStatusSentCode)) {
                message.setSentDate(new Timestamp(System.currentTimeMillis()));
            }
        }

        message.setOperator(operatorNumber.getOperator());
        message.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        message.setIsRead(false);

        messageService.save(message);

        if (message.getMessageStatus().getCode().equals(Util.messageStatusDelayedCode)) {
            return ResponseEntity.badRequest().body("Зурвас илгээлт амжилтгүй!");
        }

        return ResponseEntity.ok().body(message);
    }

    @PostMapping("/sendByApi")
    public ResponseEntity<?> sendByApi(@RequestBody MessageDTO messageDTO) {
        if (messageDTO.getNumbers() == null || messageDTO.getNumbers().size() < 1) {
            return ResponseEntity.badRequest().body("Утасны дугаар хоосон байна!");
        }

        if (messageDTO.getContent() == null || messageDTO.getContent().equals("")) {
            return ResponseEntity.badRequest().body("Агуулга хоосон байна!");
        }

        for (Long number: messageDTO.getNumbers()) {
            Message message = new Message();

            OperatorNumber operatorNumber = operatorNumberService.findByNumber(Long.valueOf(number.toString().substring(0, 2)));

            if (operatorNumber == null) {
                return ResponseEntity.badRequest().body("Үүрэн холбоо олдсонгүй!");
            }

            MessageStatus messageStatus = messageStatusService.findByCode(Util.messageStatusScheduledCode);

            if (messageDTO.getScheduleDate() != null) {
                message.setScheduleDate(messageDTO.getScheduleDate());
                message.setMessageStatus(messageStatus);
            } else {
                String messageStatusCode = messageService.send(operatorNumber.getOperator().getName(), number, messageDTO.getContent());

                messageStatus = messageStatusService.findByCode(messageStatusCode);
                message.setMessageStatus(messageStatus);

                if (messageStatusCode.equals(Util.messageStatusSentCode)) {
                    message.setSentDate(new Timestamp(System.currentTimeMillis()));
                }
            }

            message.setNumber(number);
            message.setContent(messageDTO.getContent());
            message.setOperator(operatorNumber.getOperator());
            message.setCreatedDate(new Timestamp(System.currentTimeMillis()));
            message.setIsRead(false);

            messageService.save(message);

            if (message.getMessageStatus().getCode().equals(Util.messageStatusDelayedCode)) {
                return ResponseEntity.badRequest().body("Зурвас илгээлт амжилтгүй!");
            }
        }

        return ResponseEntity.ok().body(messageDTO);
    }

    @Scheduled(fixedDelay = 60000)
    @GetMapping("/schedule")
    public void schedule() {
        MessageStatus messageStatusByScheduledCode = messageStatusService.findByCode(Util.messageStatusScheduledCode);

        List<Message> messages = messageService.findByMessageStatus(messageStatusByScheduledCode);

        for (Message message : messages) {
            if (message.getScheduleDate().getTime() <= new Timestamp(System.currentTimeMillis()).getTime()) {
                String messageStatusCode = messageService.send(message.getOperator().getName(), message.getNumber(), message.getContent());
                MessageStatus messageStatus = messageStatusService.findByCode(messageStatusCode);
                message.setMessageStatus(messageStatus);

                if (messageStatusCode.equals(Util.messageStatusSentCode)) {
                    message.setSentDate(new Timestamp(System.currentTimeMillis()));
                }

                messageService.save(message);
            }
        }
    }
}
