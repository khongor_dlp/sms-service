package com.vertexmon.sms.controller;

import com.vertexmon.sms.model.Message;
import com.vertexmon.sms.model.MessageStatus;
import com.vertexmon.sms.model.MessageToken;
import com.vertexmon.sms.model.OperatorNumber;
import com.vertexmon.sms.service.MessageService;
import com.vertexmon.sms.service.MessageStatusService;
import com.vertexmon.sms.service.MessageTokenService;
import com.vertexmon.sms.service.OperatorNumberService;
import com.vertexmon.sms.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;

@RestController
@RequestMapping("messageToken")
public class MessageTokenController {

    @Autowired
    MessageTokenService messageTokenService;

    @Autowired
    MessageService messageService;

    @Autowired
    OperatorNumberService operatorNumberService;

    @Autowired
    MessageStatusService messageStatusService;

    @PostMapping("/generate")
    public ResponseEntity<?> generate(@RequestBody MessageToken messageToken) {
        if (messageToken.getNumber() == null || messageToken.getNumber().toString().length() != 8) {
            return ResponseEntity.badRequest().body("Утасны дугаар хоосон байна!");
        }

        OperatorNumber operatorNumber = operatorNumberService.findByNumber(Long.valueOf(messageToken.getNumber().toString().substring(0, 2)));

        if (operatorNumber == null) {
            return ResponseEntity.badRequest().body("Үүрэн холбоо олдсонгүй!");
        }

        messageToken = messageTokenService.generate(messageToken.getNumber());

        String messageStatusCode = messageService.send(operatorNumber.getOperator().getName(), messageToken.getNumber(), messageToken.getCode());

        MessageStatus messageStatus = messageStatusService.findByCode(messageStatusCode);

        Message message = new Message();

        message.setMessageStatus(messageStatus);

        if (messageStatusCode.equals(Util.messageStatusSentCode)) {
            message.setSentDate(new Timestamp(System.currentTimeMillis()));
        }

        message.setNumber(messageToken.getNumber());
        message.setContent(messageToken.getCode());
        message.setOperator(operatorNumber.getOperator());
        message.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        message.setIsRead(false);

        messageService.save(message);

        if (message.getMessageStatus().getCode().equals(Util.messageStatusDelayedCode)) {
            return ResponseEntity.badRequest().body("Зурвас илгээлт амжилтгүй!");
        }

        return ResponseEntity.ok().body(message);
    }

    @PostMapping("/check")
    public ResponseEntity<?> check(@RequestBody MessageToken messageToken) {
        if (messageToken.getNumber() == null || messageToken.getNumber().toString().length() != 8) {
            return ResponseEntity.badRequest().body(false);
        }

        if (messageToken.getCode() == null || messageToken.getCode().length() != 4) {
            return ResponseEntity.badRequest().body(false);
        }

        boolean result = messageTokenService.check(messageToken.getCode(), messageToken.getNumber());

        if (!result) {
            return ResponseEntity.badRequest().body(false);
        }

        return ResponseEntity.ok().body(true);
    }
}
