package com.vertexmon.sms.controller;

import com.sipios.springsearch.anotation.SearchSpec;
import com.vertexmon.sms.model.MessageBot;
import com.vertexmon.sms.service.MessageBotService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("messageBot")
public class MessageBotController {

    @Autowired
    MessageBotService messageBotService;

    @GetMapping("/get")
    public ResponseEntity<?> get(@SearchSpec Specification<MessageBot> specs,
                                 Pageable pageable) {
        Page<MessageBot> messageBots = messageBotService.get(specs, pageable);

        return ResponseEntity.ok().body(messageBots);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<?> getById(@PathVariable(value = "id") Long id) {
        MessageBot messageBot = messageBotService.getById(id);

        return ResponseEntity.ok().body(messageBot);
    }

    @PostMapping("/save")
    public ResponseEntity<?> save(@RequestBody MessageBot messageBot) {
        if (messageBot.getQuestion() == null || messageBot.getQuestion().equals("")) {
            return ResponseEntity.badRequest().body("Асуулт хоосон байна!");
        }

        if (messageBot.getAnswer() == null || messageBot.getAnswer().equals("")) {
            return ResponseEntity.badRequest().body("Хариулт хоосон байна");
        }

        messageBot = messageBotService.save(messageBot);

        return ResponseEntity.ok().body(messageBot);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> update(@PathVariable(value = "id") Long id,
                                    @RequestBody MessageBot messageBot) {
        if (messageBot.getQuestion() == null || messageBot.getQuestion().equals("")) {
            return ResponseEntity.badRequest().body("Асуулт хоосон байна!");
        }

        if (messageBot.getAnswer() == null || messageBot.getAnswer().equals("")) {
            return ResponseEntity.badRequest().body("Хариулт хоосон байна");
        }

        MessageBot messageBot1 = messageBotService.getById(id);

        messageBot1.setQuestion(messageBot.getQuestion());
        messageBot1.setAnswer(messageBot.getAnswer());

        messageBotService.save(messageBot1);

        return ResponseEntity.ok().body(messageBot1);
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<Boolean> delete(@PathVariable(value = "id") Long id) {
        messageBotService.delete(id);

        return ResponseEntity.ok().body(true);
    }
}
