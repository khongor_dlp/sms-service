package com.vertexmon.sms.controller;

import com.sipios.springsearch.anotation.SearchSpec;
import com.vertexmon.sms.model.Company;
import com.vertexmon.sms.service.CompanyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("company")
public class CompanyController {

    @Autowired
    CompanyService companyService;

    @GetMapping("/get")
    public ResponseEntity<?> get(@SearchSpec Specification<Company> specs,
                                 Pageable pageable) {
        Page<Company> companies = companyService.get(specs, pageable);

        log.info("Authentication: " + SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal());

        return ResponseEntity.ok().body(companies);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<?> getById(@PathVariable(value = "id") Long id) {
        Company company = companyService.getById(id);

        return ResponseEntity.ok().body(company);
    }

    @PostMapping("/save")
    public ResponseEntity<?> save(@RequestBody Company company) {
        if (company.getName() == null || company.getName().equals("")) {
            return ResponseEntity.badRequest().body("Нэр хоосон байна!");
        }

        if (company.getKeyword() == null || company.getKeyword().equals("")) {
            return ResponseEntity.badRequest().body("Түлхүүр үг хоосон байна!");
        }

        company = companyService.save(company);

        return ResponseEntity.ok().body(company);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> update(@PathVariable(value = "id") Long id,
                                    @RequestBody Company company) {
        if (company.getName() == null || company.getName().equals("")) {
            return ResponseEntity.badRequest().body("Нэр хоосон байна!");
        }

        if (company.getKeyword() == null || company.getKeyword().equals("")) {
            return ResponseEntity.badRequest().body("Түлхүүр үг хоосон байна!");
        }

        Company company1 = companyService.getById(id);

        company1.setName(company.getName());
        company1.setKeyword(company.getKeyword());

        companyService.save(company1);

        return ResponseEntity.ok().body(company1);
    }
}
