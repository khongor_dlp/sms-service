package com.vertexmon.sms.controller;

import com.sipios.springsearch.anotation.SearchSpec;
import com.vertexmon.sms.model.Company;
import com.vertexmon.sms.model.User;
import com.vertexmon.sms.model.UserRole;
import com.vertexmon.sms.dto.TokenDTO;
import com.vertexmon.sms.service.CompanyService;
import com.vertexmon.sms.service.UserRoleService;
import com.vertexmon.sms.service.UserService;
import com.vertexmon.sms.util.JwtTokenUtil;
import com.vertexmon.sms.util.Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    UserRoleService userRoleService;

    @Autowired
    CompanyService companyService;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @Autowired
    PasswordEncoder passwordEncoder;

    @GetMapping("/get")
    public ResponseEntity<?> get(@SearchSpec Specification<User> specs,
                                 Pageable pageable) {
        Page<User> users = userService.get(specs, pageable);

        return ResponseEntity.ok().body(users);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<?> getById(@PathVariable(value = "id") Long id) {
        User user = userService.getById(id);

        return ResponseEntity.ok().body(user);
    }

    @PostMapping("/save")
    public ResponseEntity<?> save(@RequestBody User user) {
        if (user.getFirstname() == null || user.getFirstname().equals("")) {
            return ResponseEntity.badRequest().body("Нэр хоосон байна!");
        }

        if (user.getLastname() == null || user.getLastname().equals("")) {
            return ResponseEntity.badRequest().body("Овог хоосон байна!");
        }

        if (user.getUsername() == null || user.getUsername().equals("")) {
            return ResponseEntity.badRequest().body("Хэрэглэгчийн нэр хоосон байна!");
        }

        if (userService.existByUsername(user.getUsername())) {
            return ResponseEntity.badRequest().body("Хэрэглэгчийн нэр үүссэн байна!");
        }

        if (user.getPassword() == null || user.getPassword().equals("")) {
            return ResponseEntity.badRequest().body("Нууц үг хоосон байна!");
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        if (user.getUserRole() == null) {
            return ResponseEntity.badRequest().body("Хэрэглэгчийн эрх сонгоогүй байна!");
        }

        UserRole userRole = userRoleService.getById(user.getUserRole().getId());

        if (userRole == null) {
            return ResponseEntity.badRequest().body("Хэрэглэгчийн эрх олдсонгүй!");
        }

        user.setUserRole(userRole);

        if (userRole.getCode().equals(Util.userRoleUserCode)) {
            if (user.getCompany() == null) {
                return ResponseEntity.badRequest().body("Компани сонгоогүй байна!");
            }

            Company company = companyService.getById(user.getCompany().getId());

            if (company == null) {
                return ResponseEntity.badRequest().body("Компани олдсонгүй!");
            }

            user.setCompany(company);
        } else if (userRole.getCode().equals(Util.userRoleAdminCode)) {
            user.setCompany(null);
        }

        user = userService.save(user);

        return ResponseEntity.ok().body(user);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> update(@PathVariable(value = "id") Long id,
                                    @RequestBody User user) {
        if (user.getFirstname() == null || user.getFirstname().equals("")) {
            return ResponseEntity.badRequest().body("Нэр хоосон байна!");
        }

        if (user.getLastname() == null || user.getLastname().equals("")) {
            return ResponseEntity.badRequest().body("Овог хоосон байна!");
        }

        if (user.getUsername() == null || user.getUsername().equals("")) {
            return ResponseEntity.badRequest().body("Хэрэглэгчийн нэр хоосон байна!");
        }

        if (user.getUserRole() == null) {
            return ResponseEntity.badRequest().body("Хэрэглэгчийн эрх сонгоогүй байна!");
        }

        UserRole userRole = userRoleService.getById(user.getUserRole().getId());

        if (userRole == null) {
            return ResponseEntity.badRequest().body("Хэрэглэгчийн эрх олдсонгүй!");
        }

        user.setUserRole(userRole);

        if (userRole.getCode().equals(Util.userRoleUserCode)) {
            if (user.getCompany() == null) {
                return ResponseEntity.badRequest().body("Компани сонгоогүй байна!");
            }

            Company company = companyService.getById(user.getCompany().getId());

            if (company == null) {
                return ResponseEntity.badRequest().body("Компани олдсонгүй!");
            }

            user.setCompany(company);
        } else if (userRole.getCode().equals(Util.userRoleAdminCode)) {
            user.setCompany(null);
        }

        User user1 = userService.getById(id);

        if (!user.getUsername().equals(user1.getUsername())) {
            if (userService.existByUsername(user.getUsername())) {
                return ResponseEntity.badRequest().body("Хэрэглэгчийн нэр үүссэн байна!");
            }
        }

        user1.setFirstname(user.getFirstname());
        user1.setLastname(user.getLastname());
        user1.setUsername(user.getUsername());
        user1.setUserRole(user.getUserRole());
        user1.setCompany(user.getCompany());

        if (user.getPassword() != null && !user.getPassword().equals("")) {
            user1.setPassword(passwordEncoder.encode(user.getPassword()));
        }

        userService.save(user1);

        return ResponseEntity.ok().body(user1);
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody User user) throws Exception {
        if (user.getUsername() == null || user.getUsername().equals("")) {
            log.info("Username not found");
            return ResponseEntity.badRequest().body("Хэрэглэгийн нэр хоосон байна!");
        }

        if (user.getPassword() == null || user.getPassword().equals("")) {
            log.info("Password not found");
            return ResponseEntity.badRequest().body("Нууц үг хоосон байна!");
        }

        User user1 = userService.getByUsername(user.getUsername());

        if (user1 == null) {
            log.info("Хэрэглэгч олдсонгүй!");
            return ResponseEntity.badRequest().body("Хэрэглэгч олдсонгүй!");
        }

        if (!passwordEncoder.matches(user.getPassword(), user1.getPassword())) {
            return ResponseEntity.badRequest().body("Нууц үг буруу байна!");
        }

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword()));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }

        final UserDetails userDetails = userService
                .loadUserByUsername(user.getUsername());

        final String token = jwtTokenUtil.generateToken(userDetails);

        TokenDTO tokenDTO = new TokenDTO();

        tokenDTO.setUserId(user1.getId());
        tokenDTO.setUserRoleId(user1.getUserRole().getId());
        tokenDTO.setUserRoleCode(user1.getUserRole().getCode());
        tokenDTO.setToken("Bearer " + token);

        return ResponseEntity.ok().body(tokenDTO);
    }
}
