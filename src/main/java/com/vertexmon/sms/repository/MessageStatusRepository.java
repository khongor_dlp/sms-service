package com.vertexmon.sms.repository;

import com.vertexmon.sms.model.MessageStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface MessageStatusRepository extends JpaRepository<MessageStatus, Long>, JpaSpecificationExecutor<MessageStatus> {

    MessageStatus findByCode(String code);
}
