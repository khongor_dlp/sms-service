package com.vertexmon.sms.repository;

import com.vertexmon.sms.model.OperatorNumber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface OperatorNumberRepository extends JpaRepository<OperatorNumber, Long>, JpaSpecificationExecutor<OperatorNumber> {

    OperatorNumber findByNumber(Long number);
}
