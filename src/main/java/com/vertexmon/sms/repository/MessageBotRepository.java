package com.vertexmon.sms.repository;

import com.vertexmon.sms.model.MessageBot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface MessageBotRepository extends JpaRepository<MessageBot, Long>, JpaSpecificationExecutor<MessageBot> {

    MessageBot findByQuestion(String question);
}
