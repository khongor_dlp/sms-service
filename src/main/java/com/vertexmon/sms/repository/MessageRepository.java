package com.vertexmon.sms.repository;

import com.vertexmon.sms.model.Message;
import com.vertexmon.sms.model.MessageStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface MessageRepository extends JpaRepository<Message, Long>, JpaSpecificationExecutor<Message> {
    List<Message> findByMessageStatus(MessageStatus messageStatus);

    Page<Message> getByMessageStatus(MessageStatus messageStatus, Pageable pageable);
}
