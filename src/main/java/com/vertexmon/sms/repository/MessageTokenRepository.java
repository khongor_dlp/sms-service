package com.vertexmon.sms.repository;

import com.vertexmon.sms.model.MessageToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageTokenRepository extends JpaRepository<MessageToken, Long>, JpaSpecificationExecutor<MessageToken> {
    MessageToken findByCodeAndNumber(String code, Long number);
}
