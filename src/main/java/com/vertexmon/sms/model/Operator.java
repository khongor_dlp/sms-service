package com.vertexmon.sms.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Operator {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
}
