package com.vertexmon.sms.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long number;
    private String content;
    private Boolean isRead;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp createdDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp sentDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp scheduleDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "message_statusid")
    private MessageStatus messageStatus;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "operatorid")
    public Operator operator;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "companyid")
    private Company company;
}
