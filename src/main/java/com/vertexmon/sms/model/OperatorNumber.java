package com.vertexmon.sms.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class OperatorNumber {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long number;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "operatorid")
    private Operator operator;
}
