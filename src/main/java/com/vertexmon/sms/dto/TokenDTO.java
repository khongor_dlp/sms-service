package com.vertexmon.sms.dto;

import lombok.Data;

@Data
public class TokenDTO {

    private String token;
    private Long userId;
    private Long userRoleId;
    private String userRoleCode;
}
