package com.vertexmon.sms.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Util {
    public String messageStatusReceivedCode = "received";
    public String messageStatusSentCode = "sent";
    public String messageStatusDelayedCode = "delayed";
    public String messageStatusScheduledCode = "scheduled";

    public String userRoleAdminCode = "admin";
    public String userRoleUserCode = "user";
}
