package com.vertexmon.sms.service;

import com.vertexmon.sms.model.OperatorNumber;
import com.vertexmon.sms.repository.OperatorNumberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OperatorNumberService {

    @Autowired
    OperatorNumberRepository operatorNumberRepository;

    public Page<OperatorNumber> get(Specification<OperatorNumber> specs, Pageable pageable) {
        return operatorNumberRepository.findAll(Specification.where(specs), pageable);
    }

    public List<OperatorNumber> findAll() {
        return operatorNumberRepository.findAll();
    }

    public OperatorNumber findByNumber(Long number) {
        return operatorNumberRepository.findByNumber(number);
    }
}
