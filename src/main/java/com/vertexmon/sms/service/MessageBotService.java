package com.vertexmon.sms.service;

import com.vertexmon.sms.model.MessageBot;
import com.vertexmon.sms.repository.MessageBotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
public class MessageBotService {

    @Autowired
    MessageBotRepository messageBotRepository;

    public Page<MessageBot> get(Specification<MessageBot> specs, Pageable pageable) {
        return messageBotRepository.findAll(Specification.where(specs), pageable);
    }

    public MessageBot getById(Long id) {
        return messageBotRepository.getOne(id);
    }

    public MessageBot findByQuestion(String question) {
        return messageBotRepository.findByQuestion(question);
    }

    public MessageBot save(MessageBot messageBot) {
        return messageBotRepository.save(messageBot);
    }

    public void delete(Long id) {
        messageBotRepository.deleteById(id);
    }
}
