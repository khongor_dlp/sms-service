package com.vertexmon.sms.service;

import com.vertexmon.sms.model.Company;
import com.vertexmon.sms.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
public class CompanyService {

    @Autowired
    CompanyRepository companyRepository;

    public Page<Company> get(Specification<Company> specs, Pageable pageable) {
        return companyRepository.findAll(Specification.where(specs), pageable);
    }

    public Company getById(Long id) {
        return companyRepository.getOne(id);
    }

    public Company findByKeyword(String keyword) {
        return companyRepository.findByKeyword(keyword);
    }

    public Company save(Company company) {
        return companyRepository.save(company);
    }
}
