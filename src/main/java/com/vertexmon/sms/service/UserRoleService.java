package com.vertexmon.sms.service;

import com.vertexmon.sms.model.UserRole;
import com.vertexmon.sms.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
public class UserRoleService {

    @Autowired
    UserRoleRepository userRoleRepository;

    public Page<UserRole> get(Specification<UserRole> specs, Pageable pageable) {
        return userRoleRepository.findAll(Specification.where(specs), pageable);
    }

    public UserRole getById(Long id) {
        return userRoleRepository.getOne(id);
    }

    public UserRole save(UserRole userRole) {
        return userRoleRepository.save(userRole);
    }
}
