package com.vertexmon.sms.service;

import com.vertexmon.sms.model.MessageStatus;
import com.vertexmon.sms.repository.MessageStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
public class MessageStatusService {

    @Autowired
    MessageStatusRepository messageStatusRepository;

    public Page<MessageStatus> get(Specification<MessageStatus> specs, Pageable pageable) {
        return messageStatusRepository.findAll(Specification.where(specs), pageable);
    }

    public MessageStatus findByCode(String code) {
        return messageStatusRepository.findByCode(code);
    }
}
