package com.vertexmon.sms.service;

import com.vertexmon.sms.model.MessageToken;
import com.vertexmon.sms.repository.MessageTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Service
public class MessageTokenService {

    @Autowired
    MessageTokenRepository messageTokenRepository;

    public MessageToken generate(Long number) {
        MessageToken messageToken = new MessageToken();

        messageToken.setCode(String.format("%04d", new Random().nextInt(10000)));
        messageToken.setNumber(number);
        messageToken.setIsConfirm(false);
        messageToken.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        messageToken.setExpireDate(new Timestamp(System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5)));

        messageTokenRepository.save(messageToken);

        return messageToken;
    }

    public boolean check(String code, Long number) {
        MessageToken messageToken = messageTokenRepository.findByCodeAndNumber(code, number);

        if (messageToken == null) {
            return false;
        }

        if (messageToken.getExpireDate().getTime() < new Timestamp(System.currentTimeMillis()).getTime()) {
            return false;
        }

        messageToken.setIsConfirm(true);

        messageTokenRepository.save(messageToken);

        return true;
    }
}
