package com.vertexmon.sms.service;

import com.vertexmon.sms.model.Operator;
import com.vertexmon.sms.repository.OperatorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
public class OperatorService {

    @Autowired
    OperatorRepository operatorRepository;

    public Page<Operator> get(Specification<Operator> specs, Pageable pageable) {
        return operatorRepository.findAll(Specification.where(specs), pageable);
    }
}
