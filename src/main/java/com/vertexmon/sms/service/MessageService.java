package com.vertexmon.sms.service;

import com.vertexmon.sms.model.Message;
import com.vertexmon.sms.model.MessageStatus;
import com.vertexmon.sms.repository.MessageRepository;
import com.vertexmon.sms.util.Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Slf4j
@Service
public class MessageService {

    @Autowired
    MessageRepository messageRepository;

    public Page<Message> get(Specification<Message> specs, Pageable pageable) {
        return messageRepository.findAll(Specification.where(specs), pageable);
    }

    public Message getById(Long id) {
        return messageRepository.getOne(id);
    }

    public Message save(Message message) {
        return messageRepository.save(message);
    }

    public List<Message> findByMessageStatus(MessageStatus messageStatus) {
        return messageRepository.findByMessageStatus(messageStatus);
    }

    public String send(String operatorName, Long number, String content) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            String messageResponse;

            switch (operatorName) {
                case "Mobicom":
                    messageResponse = restTemplate.getForObject("http://27.123.214.168/smsmt/mt?servicename=Chanartai&username=Dugaar&from=130301&to=" + number + "&msg=" + content, String.class);

                    if (messageResponse != null && messageResponse.toLowerCase().contains("sent")) {
                        return Util.messageStatusSentCode;
                    }

                    break;
                case "Unitel":
                    messageResponse = restTemplate.getForObject("http://sms.unitel.mn/sendSMS.php?uname=dlp&upass=S9pWYY2e1R&sms=" + content + "&from=134444&mobile=" + number, String.class);

                    if (messageResponse != null && messageResponse.toLowerCase().contains("success")) {
                        return Util.messageStatusSentCode;
                    }

                    break;
                case "Skytel":
                    messageResponse = restTemplate.getForObject("http://smsgw.skytel.mn:80/SMSGW-war/pushsms?id=1000121&src=134444&dest=" + number + "&text=" + content, String.class);

                    if (messageResponse != null && messageResponse.toLowerCase().contains("ok")) {
                        return Util.messageStatusSentCode;
                    }

                    break;
                case "Gmobile":
                    messageResponse = restTemplate.getForObject("http://203.91.114.131/cgi-bin/sendsms?username=di_elpi&password=delpi_444&from=134444&to=" + number + "&text=" + content, String.class);

                    if (messageResponse != null && messageResponse.toLowerCase().contains("accepted for delivery")) {
                        return Util.messageStatusSentCode;
                    }

                    break;
                default:
                    break;
            }

            return Util.messageStatusDelayedCode;
        } catch (Exception e) {
            log.info(e.getMessage());
            return Util.messageStatusDelayedCode;
        }
    }
}
